package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/pkg/helper"
)

type ComingProductRepo struct {
	db *pgxpool.Pool
}

func NewComingProductRepo(db *pgxpool.Pool) *ComingProductRepo {
	return &ComingProductRepo{
		db: db,
	}
}

func (r *ComingProductRepo) Create(ctx context.Context, req *stock_service.ComingProductCreate) (*stock_service.ComingProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO coming_products(id, coming_id, brand_id, category_id, product_name, barcode, quantity, coming_price)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.ComingId,
		req.BrandId,
		req.CategoryId,
		req.ProductName,
		req.Barcode,
		req.Quantity,
		req.ComingPrice,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ComingProduct{
		Id:          id,
		ComingId:    req.ComingId,
		BrandId:     req.BrandId,
		CategoryId:  req.CategoryId,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		Quantity:    req.Quantity,
		ComingPrice: req.ComingPrice,
	}, nil
}

func (r *ComingProductRepo) GetByID(ctx context.Context, req *stock_service.ComingProductPrimaryKey) (*stock_service.ComingProduct, error) {
	var (
		query string

		id          sql.NullString
		comingId    sql.NullString
		brandId     sql.NullString
		categoryId  sql.NullString
		productName string
		barcode     string
		quantity    int64
		comingPrice int64
	)

	query = `
		SELECT
			id, coming_id, brand_id, category_id, product_name, barcode, quantity, coming_price
		FROM coming_products
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&comingId,
		&brandId,
		&categoryId,
		&productName,
		&barcode,
		&quantity,
		&comingPrice,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.ComingProduct{
		Id:          id.String,
		ComingId:    comingId.String,
		BrandId:     brandId.String,
		CategoryId:  categoryId.String,
		ProductName: productName,
		Barcode:     barcode,
		Quantity:    quantity,
		ComingPrice: comingPrice,
	}, nil
}

func (r *ComingProductRepo) GetList(ctx context.Context, req *stock_service.ComingProductGetListRequest) (*stock_service.ComingProductGetListResponse, error) {

	var (
		resp   = &stock_service.ComingProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, coming_id, brand_id, category_id, product_name, barcode, quantity, coming_price	
		FROM coming_products
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SearchByBarcode + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SearchByComingId != "" {
		where += ` AND coming_id ILIKE '%' || '` + req.SearchByComingId + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			comingId    sql.NullString
			brandId     sql.NullString
			categoryId  sql.NullString
			productName string
			barcode     string
			quantity    int64
			comingPrice int64
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&comingId,
			&brandId,
			&categoryId,
			&productName,
			&barcode,
			&quantity,
			&comingPrice,
		)

		if err != nil {
			return nil, err
		}

		resp.ComingProducts = append(resp.ComingProducts, &stock_service.ComingProduct{
			Id:          id.String,
			ComingId:    comingId.String,
			BrandId:     brandId.String,
			CategoryId:  categoryId.String,
			ProductName: productName,
			Barcode:     barcode,
			Quantity:    quantity,
			ComingPrice: comingPrice,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ComingProductRepo) Update(ctx context.Context, req *stock_service.ComingProductUpdate) (*stock_service.ComingProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			coming_products
		SET
			coming_id = :coming_id, 
			brand_id = :brand_id, 
			category_id = :category_id, 
			product_name = :product_name, 
			barcode = :barcode, 
			quantity = :quantity, 
			coming_price = :coming_price	
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"coming_id":    req.ComingId,
		"brand_id":     req.BrandId,
		"category_id":  req.CategoryId,
		"product_name": req.ProductName,
		"barcode":      req.Barcode,
		"quantity":     req.Quantity,
		"coming_price": req.ComingPrice,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &stock_service.ComingProduct{
		Id:          req.Id,
		ComingId:    req.ComingId,
		BrandId:     req.BrandId,
		CategoryId:  req.CategoryId,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		Quantity:    req.Quantity,
		ComingPrice: req.ComingPrice,
	}, nil
}

func (r *ComingProductRepo) Delete(ctx context.Context, req *stock_service.ComingProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM coming_products WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
