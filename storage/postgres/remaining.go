package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/pkg/helper"
)

type RemainingRepo struct {
	db *pgxpool.Pool
}

func NewRemainingRepo(db *pgxpool.Pool) *RemainingRepo {
	return &RemainingRepo{
		db: db,
	}
}

func (r *RemainingRepo) Create(ctx context.Context, req *stock_service.RemainingCreate) (*stock_service.Remaining, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO remaining(id, branch_id, brand_id, category_id, product_name, barcode, coming_price, quantity)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.BrandId,
		req.CategoryId,
		req.ProductName,
		req.Barcode,
		req.ComingPrice,
		req.Quantity,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.Remaining{
		Id:          id,
		BranchId:    req.BranchId,
		BrandId:     req.BrandId,
		CategoryId:  req.CategoryId,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		ComingPrice: req.ComingPrice,
		Quantity:    req.Quantity,
	}, nil
}

func (r *RemainingRepo) GetByID(ctx context.Context, req *stock_service.RemainingPrimaryKey) (*stock_service.Remaining, error) {
	var (
		query string
		where string

		id          sql.NullString
		branchId    sql.NullString
		brandId     sql.NullString
		categoryId  sql.NullString
		productName string
		barcode     string
		comingPrice int64
		quantity    int64
	)

	query = `
		SELECT
			id, branch_id, brand_id, category_id, product_name, barcode, coming_price, quantity
		FROM remaining
	`
	if len(req.ProductName) > 0 {
		where = " WHERE product_name = $1"
		req.Id = req.ProductName
	} else if len(req.Id) > 0 {
		where = " WHERE id = $1"
	}

	query += where

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branchId,
		&brandId,
		&categoryId,
		&productName,
		&barcode,
		&comingPrice,
		&quantity,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.Remaining{
		Id:          id.String,
		BranchId:    branchId.String,
		BrandId:     brandId.String,
		CategoryId:  categoryId.String,
		ProductName: productName,
		Barcode:     barcode,
		ComingPrice: comingPrice,
		Quantity:    quantity,
	}, nil
}

func (r *RemainingRepo) GetList(ctx context.Context, req *stock_service.RemainingGetListRequest) (*stock_service.RemainingGetListResponse, error) {

	var (
		resp   = &stock_service.RemainingGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, branch_id, brand_id, category_id, product_name, barcode, coming_price, quantity
		FROM remaining
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByBarcode != "" {
		where += ` AND barcode ILIKE '%' || '` + req.SearchByBarcode + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branchId    sql.NullString
			brandId     sql.NullString
			categoryId  sql.NullString
			productName string
			barcode     string
			comingPrice int64
			quantity    int64
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branchId,
			&brandId,
			&categoryId,
			&productName,
			&barcode,
			&comingPrice,
			&quantity,
		)

		if err != nil {
			return nil, err
		}

		resp.Remaining = append(resp.Remaining, &stock_service.Remaining{
			Id:          id.String,
			BranchId:    branchId.String,
			BrandId:     brandId.String,
			CategoryId:  categoryId.String,
			ProductName: productName,
			Barcode:     barcode,
			ComingPrice: comingPrice,
			Quantity:    quantity,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *RemainingRepo) Update(ctx context.Context, req *stock_service.RemainingUpdate) (*stock_service.Remaining, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			remaining
		SET
		    branch_id = :branch_id,
		    brand_id = :brand_id,
		    category_id = :category_id,
		    product_name = :product_name,
		    barcode = :barcode,
		    coming_price = :coming_price,
		    quantity = :quantity
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"branch_id":    req.BranchId,
		"brand_id":     req.BrandId,
		"category_id":  req.CategoryId,
		"product_name": req.ProductName,
		"barcode":      req.Barcode,
		"coming_price": req.ComingPrice,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &stock_service.Remaining{
		Id:          req.Id,
		BranchId:    req.BranchId,
		BrandId:     req.BrandId,
		CategoryId:  req.CategoryId,
		ProductName: req.ProductName,
		Barcode:     req.Barcode,
		ComingPrice: req.ComingPrice,
		Quantity:    req.Quantity,
	}, nil
}

func (r *RemainingRepo) Delete(ctx context.Context, req *stock_service.RemainingPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM remaining WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
