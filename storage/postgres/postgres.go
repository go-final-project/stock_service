package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/stock_service/config"
	"gitlab.com/GoFinalProject/stock_service/storage"
)

type Store struct {
	db            *pgxpool.Pool
	coming        *ComingRepo
	comingProduct *ComingProductRepo
	remaining     *RemainingRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Coming() storage.ComingRepoI {
	if s.coming == nil {
		s.coming = NewComingRepo(s.db)
	}

	return s.coming
}

func (s *Store) ComingProduct() storage.ComingProductRepoI {
	if s.comingProduct == nil {
		s.comingProduct = NewComingProductRepo(s.db)
	}

	return s.comingProduct
}

func (s *Store) Remaining() storage.RemainingRepoI {
	if s.remaining == nil {
		s.remaining = NewRemainingRepo(s.db)
	}

	return s.remaining
}
