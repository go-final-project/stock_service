package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/pkg/helper"
)

type ComingRepo struct {
	db *pgxpool.Pool
}

func NewComingRepo(db *pgxpool.Pool) *ComingRepo {
	return &ComingRepo{
		db: db,
	}
}

func (r *ComingRepo) Create(ctx context.Context, req *stock_service.ComingCreate) (*stock_service.Coming, error) {
	count, err := r.GetList(ctx, &stock_service.ComingGetListRequest{})
	var (
		id       = uuid.New().String()
		comingId = helper.MakeId("П", int(count.Count)+1)
		query    string
	)

	query = `
		INSERT INTO coming(id, coming_id, branch_id, provider_id, date_time, status)
		VALUES ($1, $2, $3, $4, $5, $6)
	`

	_, err = r.db.Exec(ctx, query,
		id,
		comingId,
		req.BranchId,
		req.ProviderId,
		req.DateTime,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.Coming{
		Id:         id,
		ComingId:   comingId,
		BranchId:   req.BranchId,
		ProviderId: req.ProviderId,
		DateTime:   req.DateTime,
		Status:     req.Status,
	}, nil
}

func (r *ComingRepo) GetByID(ctx context.Context, req *stock_service.ComingPrimaryKey) (*stock_service.Coming, error) {
	var (
		query string

		id         sql.NullString
		branchId   sql.NullString
		providerId sql.NullString
		comingId   string
		dateTime   string
		status     string
	)

	query = `
		SELECT
			id, coming_id, branch_id, provider_id, date_time, status
		FROM coming
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&comingId,
		&branchId,
		&providerId,
		&dateTime,
		&status,
	)

	if err != nil {
		return nil, err
	}

	return &stock_service.Coming{
		Id:         id.String,
		ComingId:   comingId,
		BranchId:   branchId.String,
		ProviderId: providerId.String,
		DateTime:   dateTime,
		Status:     status,
	}, nil
}

func (r *ComingRepo) GetList(ctx context.Context, req *stock_service.ComingGetListRequest) (*stock_service.ComingGetListResponse, error) {

	var (
		resp   = &stock_service.ComingGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, coming_id, branch_id, provider_id, date_time, status
		FROM coming
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			branchId   sql.NullString
			providerId sql.NullString
			comingId   string
			dateTime   string
			status     string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&comingId,
			&dateTime,
			&branchId,
			&providerId,
			&status,
		)

		if err != nil {
			return nil, err
		}

		resp.Comings = append(resp.Comings, &stock_service.Coming{
			Id:         id.String,
			ComingId:   comingId,
			BranchId:   branchId.String,
			ProviderId: providerId.String,
			DateTime:   dateTime,
			Status:     status,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ComingRepo) Update(ctx context.Context, req *stock_service.ComingUpdate) (*stock_service.Coming, error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
		UPDATE
			coming
		SET
		    coming_id = :coming_id, 
		    branch_id = :branch_id, 
		    provider_id = :provider_id, 
		    date_time = :date_time,
		    status = :status
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.Id,
		"coming_id":   req.ComingId,
		"branch_id":   req.BranchId,
		"provider_id": req.ProviderId,
		"date_time":   req.DateTime,
		"status":      req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &stock_service.Coming{
		Id:         req.Id,
		ComingId:   req.ComingId,
		BranchId:   req.BranchId,
		ProviderId: req.ProviderId,
		DateTime:   req.DateTime,
		Status:     req.Status,
	}, nil
}

func (r *ComingRepo) Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM coming WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
