package storage

import (
	"context"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
)

type StorageI interface {
	CloseDB()
	Coming() ComingRepoI
	ComingProduct() ComingProductRepoI
	Remaining() RemainingRepoI
}

type ComingRepoI interface {
	Create(context.Context, *stock_service.ComingCreate) (*stock_service.Coming, error)
	GetByID(context.Context, *stock_service.ComingPrimaryKey) (*stock_service.Coming, error)
	GetList(context.Context, *stock_service.ComingGetListRequest) (*stock_service.ComingGetListResponse, error)
	Update(context.Context, *stock_service.ComingUpdate) (*stock_service.Coming, error)
	Delete(context.Context, *stock_service.ComingPrimaryKey) error
}

type ComingProductRepoI interface {
	Create(context.Context, *stock_service.ComingProductCreate) (*stock_service.ComingProduct, error)
	GetByID(context.Context, *stock_service.ComingProductPrimaryKey) (*stock_service.ComingProduct, error)
	GetList(context.Context, *stock_service.ComingProductGetListRequest) (*stock_service.ComingProductGetListResponse, error)
	Update(context.Context, *stock_service.ComingProductUpdate) (*stock_service.ComingProduct, error)
	Delete(context.Context, *stock_service.ComingProductPrimaryKey) error
}

type RemainingRepoI interface {
	Create(context.Context, *stock_service.RemainingCreate) (*stock_service.Remaining, error)
	GetByID(context.Context, *stock_service.RemainingPrimaryKey) (*stock_service.Remaining, error)
	GetList(context.Context, *stock_service.RemainingGetListRequest) (*stock_service.RemainingGetListResponse, error)
	Update(context.Context, *stock_service.RemainingUpdate) (*stock_service.Remaining, error)
	Delete(context.Context, *stock_service.RemainingPrimaryKey) error
}
