CREATE TABLE coming_products (
    id UUID PRIMARY KEY,
    coming_id UUID REFERENCES coming("id"),
    brand_id UUID NOT NULL,
    category_id UUID NOT NULL,
    product_name VARCHAR NOT NULL,
    barcode VARCHAR NOT NULL,
    quantity NUMERIC,
    coming_price NUMERIC
);