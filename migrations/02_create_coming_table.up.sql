CREATE TABLE coming (
    id UUID PRIMARY KEY,
    coming_id VARCHAR(1),
    branch_id UUID NOT NULL,
    provider_id UUID NOT NULL,
    date_time date,
    status VARCHAR DEFAULT 'in_process'
);