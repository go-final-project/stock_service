CREATE TABLE remaining (
    id UUID PRIMARY KEY,
    branch_id UUID NOT NULL,
    brand_id UUID NOT NULL,
    category_id UUID NOT NULL,
    product_name VARCHAR NOT NULL,
    barcode VARCHAR NOT NULL,
    coming_price NUMERIC NOT NULL,
    quantity NUMERIC NOT NULL
);