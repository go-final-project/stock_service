package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/stock_service/config"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/grpc/client"
	"gitlab.com/GoFinalProject/stock_service/pkg/logger"
	"gitlab.com/GoFinalProject/stock_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type RemainingService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedRemainingServiceServer
}

func NewRemainingService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *RemainingService {
	return &RemainingService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *RemainingService) Create(ctx context.Context, req *stock_service.RemainingCreate) (*stock_service.Remaining, error) {
	u.log.Info("====== Remaining Create ======", logger.Any("req", req))

	resp, err := u.strg.Remaining().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Remaining: u.strg.Remaining().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainingService) GetById(ctx context.Context, req *stock_service.RemainingPrimaryKey) (*stock_service.Remaining, error) {
	u.log.Info("====== Remaining Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Remaining().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Remaining Get By ID: u.strg.Remaining().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainingService) GetList(ctx context.Context, req *stock_service.RemainingGetListRequest) (*stock_service.RemainingGetListResponse, error) {
	u.log.Info("====== Remaining Get List ======", logger.Any("req", req))

	resp, err := u.strg.Remaining().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Remaining Get List: u.strg.Remaining().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainingService) Update(ctx context.Context, req *stock_service.RemainingUpdate) (*stock_service.Remaining, error) {
	u.log.Info("====== Remaining Update ======", logger.Any("req", req))

	resp, err := u.strg.Remaining().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Remaining Update: u.strg.Remaining().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainingService) Delete(ctx context.Context, req *stock_service.RemainingPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Remaining Delete ======", logger.Any("req", req))

	err := u.strg.Remaining().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Remaining Delete: u.strg.Remaining().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
