package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/stock_service/config"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/grpc/client"
	"gitlab.com/GoFinalProject/stock_service/pkg/logger"
	"gitlab.com/GoFinalProject/stock_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ComingProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedComingProductServiceServer
}

func NewComingProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ComingProductService {
	return &ComingProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ComingProductService) Create(ctx context.Context, req *stock_service.ComingProductCreate) (*stock_service.ComingProduct, error) {
	u.log.Info("====== ComingProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.ComingProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create ComingProduct: u.strg.ComingProduct().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingProductService) GetById(ctx context.Context, req *stock_service.ComingProductPrimaryKey) (*stock_service.ComingProduct, error) {
	u.log.Info("====== ComingProduct Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.ComingProduct().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While ComingProduct Get By ID: u.strg.ComingProduct().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingProductService) GetList(ctx context.Context, req *stock_service.ComingProductGetListRequest) (*stock_service.ComingProductGetListResponse, error) {
	u.log.Info("====== ComingProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.ComingProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While ComingProduct Get List: u.strg.ComingProduct().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingProductService) Update(ctx context.Context, req *stock_service.ComingProductUpdate) (*stock_service.ComingProduct, error) {
	u.log.Info("====== ComingProduct Update ======", logger.Any("req", req))

	resp, err := u.strg.ComingProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While ComingProduct Update: u.strg.ComingProduct().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingProductService) Delete(ctx context.Context, req *stock_service.ComingProductPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== ComingProduct Delete ======", logger.Any("req", req))

	err := u.strg.ComingProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While ComingProduct Delete: u.strg.ComingProduct().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
