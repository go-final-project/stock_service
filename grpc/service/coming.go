package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/stock_service/config"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/grpc/client"
	"gitlab.com/GoFinalProject/stock_service/pkg/logger"
	"gitlab.com/GoFinalProject/stock_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ComingService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*stock_service.UnimplementedComingServiceServer
}

func NewComingService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ComingService {
	return &ComingService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ComingService) Create(ctx context.Context, req *stock_service.ComingCreate) (*stock_service.Coming, error) {
	u.log.Info("====== Coming Create ======", logger.Any("req", req))

	resp, err := u.strg.Coming().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Coming: u.strg.Coming().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingService) GetById(ctx context.Context, req *stock_service.ComingPrimaryKey) (*stock_service.Coming, error) {
	u.log.Info("====== Coming Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Coming().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Coming Get By ID: u.strg.Coming().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingService) GetList(ctx context.Context, req *stock_service.ComingGetListRequest) (*stock_service.ComingGetListResponse, error) {
	u.log.Info("====== Coming Get List ======", logger.Any("req", req))

	resp, err := u.strg.Coming().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Coming Get List: u.strg.Coming().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingService) Update(ctx context.Context, req *stock_service.ComingUpdate) (*stock_service.Coming, error) {
	u.log.Info("====== Coming Update ======", logger.Any("req", req))

	resp, err := u.strg.Coming().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Coming Update: u.strg.Coming().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ComingService) Delete(ctx context.Context, req *stock_service.ComingPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Coming Delete ======", logger.Any("req", req))

	err := u.strg.Coming().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Coming Delete: u.strg.Coming().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
