package grpc

import (
	"gitlab.com/GoFinalProject/stock_service/config"
	"gitlab.com/GoFinalProject/stock_service/genproto/stock_service"
	"gitlab.com/GoFinalProject/stock_service/grpc/client"
	"gitlab.com/GoFinalProject/stock_service/grpc/service"
	"gitlab.com/GoFinalProject/stock_service/pkg/logger"
	"gitlab.com/GoFinalProject/stock_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	stock_service.RegisterComingProductServiceServer(grpcServer, service.NewComingProductService(cfg, log, strg, srvc))
	stock_service.RegisterComingServiceServer(grpcServer, service.NewComingService(cfg, log, strg, srvc))
	stock_service.RegisterRemainingServiceServer(grpcServer, service.NewRemainingService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
